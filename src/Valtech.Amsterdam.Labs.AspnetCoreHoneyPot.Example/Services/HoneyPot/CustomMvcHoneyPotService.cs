﻿using Microsoft.AspNetCore.Http;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Example.Services.Localization;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Models;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Services;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Example.Services.HoneyPot
{
    /// <summary>
    /// An example of how to override the <see cref="DefaultHoneyPotService"/>
    /// with some custom properties and functionalities
    /// </summary>
    public sealed class CustomHoneyPotService : DefaultHoneyPotService
    {
        private readonly ILocalizationService _localizationService;
        /// <summary>
        /// An example of how to override the <see cref="DefaultHoneyPotService"/>
        /// with some custom properties and functionalities
        /// </summary>
        public CustomHoneyPotService(IHttpContextAccessor httpContextAccessor, ILocalizationService localizationService)
            : base(httpContextAccessor)
        {
            // Add custom words to the input identifiers
            Settings.InputIdentifiers.Add("Noot");

            _localizationService = localizationService;
        }

        // Call localization service for an actual error message
        public override string GetErrorMessage() =>
            _localizationService.GetText("/Validation/HoneyPotError");

        public override HoneyPotSettings Settings => new HoneyPotSettings
        {
            InputClassName = "emphasize-example" // <-- Set to hidden or another class that doesn't display the input
        };
    }
}