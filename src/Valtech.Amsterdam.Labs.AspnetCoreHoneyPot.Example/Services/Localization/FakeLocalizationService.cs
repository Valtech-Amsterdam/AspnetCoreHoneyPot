﻿using System.Globalization;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Example.Services.Localization
{
    public class FakeLocalizationService : ILocalizationService
    {
        public string GetText(string textEntryPath) =>
            $"Localized text for {textEntryPath} " +
            $"in current culture [{CultureInfo.CurrentCulture.TwoLetterISOLanguageName}]";
    }
}