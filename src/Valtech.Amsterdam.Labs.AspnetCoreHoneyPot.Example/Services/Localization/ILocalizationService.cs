﻿namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Example.Services.Localization
{
    public interface ILocalizationService
    {
        string GetText(string textEntryPath);
    }
}