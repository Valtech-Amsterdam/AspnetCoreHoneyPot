﻿using Microsoft.AspNetCore.Mvc;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Example.Models;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Example.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index() => View(new ExampleFormViewModel());

        [HttpPost, ValidateAntiForgeryToken] // <-- So you don't need an extra attribute for every form
        public ActionResult Index(ExampleFormViewModel model) => View(model);
    }
}