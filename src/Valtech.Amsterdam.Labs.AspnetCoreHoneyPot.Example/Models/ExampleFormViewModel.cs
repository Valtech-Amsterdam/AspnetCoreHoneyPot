﻿using System.ComponentModel.DataAnnotations;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Example.Models
{
    public class ExampleFormViewModel
    {
        [Required]
        public string SomeRequiredField { get; set; }
        public string SomeOtherField { get; set; }
    }
}