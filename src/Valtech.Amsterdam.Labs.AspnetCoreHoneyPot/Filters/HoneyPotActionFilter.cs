﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Extensions;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Services;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Filters
{
    /// <summary>
    /// Action filter for checking HoneyPot values
    /// </summary>
    public class HoneyPotActionFilter : ActionFilterAttribute
    {
        /// <inherit />
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ValidateHoneyPot(filterContext);
            base.OnActionExecuting(filterContext);
        }

        private void ValidateHoneyPot(ActionExecutingContext filterContext)
        {
            var honeyPotService = filterContext.HttpContext.GetService<IHoneyPotService>();
            if (filterContext.HttpContext?.Request?.HasFormContentType != true) return;
            var requestParameters = filterContext.HttpContext.Request.Form;
            if (honeyPotService.SelectedInputIdentifier == null) return;

            if (requestParameters?.ContainsKey(honeyPotService.SelectedInputIdentifier) != true) return;
            var requestParameterValue = requestParameters[honeyPotService.SelectedInputIdentifier]
                .SingleOrDefault();
            if (String.IsNullOrWhiteSpace(requestParameterValue)) return;

            // Oh noes! You iz bot
            filterContext.ModelState
                .AddModelError(honeyPotService.Settings.ErrorKey, honeyPotService.GetErrorMessage());
        }
    }
}