﻿using System;
using System.Collections.Generic;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Models
{
    /// <summary>
    /// Settings for the HoneyPot behavior
    /// </summary>
    public class HoneyPotSettings
    {
        /// <summary>
        /// The classname to insert on the honeypot input element (Defaults to empty)
        /// </summary>
        public virtual string InputClassName { get; set; } = String.Empty;

        /// <summary>
        /// Disable the autocomplete on the input field, this is defaulted to true if you experiance any problems with bot's skipping this field
        /// you can disable this setting
        /// </summary>
        public virtual bool DisableAutoComplete { get; set; } = true;

        /// <summary>
        /// A list of possible identifier parts for the input name.
        /// Default value is heavily inspired by https://github.com/webadvanced/Honeypot-MVC />
        /// </summary>
        public virtual IList<string> InputIdentifiers => new List<string>
            {
                "User", "Name", "Age", "Question", "List", "Why",
                "Type", "Phone", "Fax", "Custom", "Relationship",
                "Friend", "Pet", "Reason"
            };

        /// <summary>
        /// The identifier for the modelstate error (Defaults to empty)
        /// </summary>
        public string ErrorKey { get; set; } = String.Empty;
    }
}
