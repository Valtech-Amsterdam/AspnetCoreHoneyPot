﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Filters;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Services;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Extensions
{
    /// <summary>
    /// HoneyPot Integration extensions
    /// </summary>
    public static class HoneyPotIntegrationExtensions
    {
        /// <summary>
        /// Simplification for getting the services required from the httpcontext
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static TService GetService<TService>(this HttpContext httpContext) where TService : class =>
            httpContext.RequestServices.GetService(typeof(TService)) as TService;

        /// <summary>
        /// Configure the application to check for the HoneyPot on when present
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public static MvcOptions AddHoneyPot(this MvcOptions config)
        {
            config.Filters.Add(new HoneyPotActionFilter());
            return config;
        }
        /// <summary>
        /// Add the required services for using the HoneyPot
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddHoneyPot(this IServiceCollection services) =>
            services.AddHoneyPot<DefaultHoneyPotService>();
        /// <summary>
        /// Add the required services for using the HoneyPot
        /// </summary>
        /// <typeparam name="THoneyPotService">Costom implementation of the HoneyPotService</typeparam>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddHoneyPot<THoneyPotService>(this IServiceCollection services)
            where THoneyPotService : class, IHoneyPotService =>
            services.AddSingleton<IHoneyPotService, THoneyPotService>();
    }
}
