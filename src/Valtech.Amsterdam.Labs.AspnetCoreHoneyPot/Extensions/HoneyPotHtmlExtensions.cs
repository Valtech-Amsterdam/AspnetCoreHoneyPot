﻿using System;
using System.IO;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Services;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Extensions
{
    /// <summary>
    /// Extension methods for using AspnetCoreHoneyPot in razor views
    /// </summary>
    public static class HoneyPotHtmlExtensions
    {
        /// <summary>
        /// Generate a honeypot input and register the identifier automatically
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
        public static HtmlString HoneyPot(this IHtmlHelper htmlHelper)
        {
            var honeyPotService = htmlHelper.ViewContext.HttpContext.GetService<IHoneyPotService>();
            var stringWriter = new StringWriter();
            var builder = new TagBuilder("input");
            if(!String.IsNullOrEmpty(honeyPotService.Settings.InputClassName))
                builder.Attributes.Add("class", honeyPotService.Settings.InputClassName);

            var inputId = honeyPotService.GetRandomInputIdentifier(); // <-- This registers automatically
            builder.Attributes.Add("name", inputId);
            builder.Attributes.Add("id", inputId);
            if(honeyPotService.Settings.DisableAutoComplete)
                builder.Attributes.Add("autocomplete", "off"); // <-- I hope this doesn't help with detecting honeypots

            builder.WriteTo(stringWriter, HtmlEncoder.Default);
            return new HtmlString(stringWriter.ToString());
        }
    }
}
