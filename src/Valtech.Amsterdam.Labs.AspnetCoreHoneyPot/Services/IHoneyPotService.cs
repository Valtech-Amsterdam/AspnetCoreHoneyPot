﻿using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Models;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Services
{
    /// <summary>
    /// The service that is responsible for managing the honeypots
    /// </summary>
    public interface IHoneyPotService
    {
        /// <summary>
        /// The settings for the AspnetCoreHoneyPot
        /// </summary>
        HoneyPotSettings Settings { get; }
        /// <summary>
        /// The input identifier currently selected in the session
        /// </summary>
        string SelectedInputIdentifier { get; }
        /// <summary>
        /// Get a random identifier and set it in the session see <see cref="SelectedInputIdentifier"/> to retreive this value.
        /// </summary>
        /// <returns></returns>
        string GetRandomInputIdentifier();
        /// <summary>
        /// A method for adding an error message to the modelstate
        /// </summary>
        /// <returns></returns>
        string GetErrorMessage();
    }
}