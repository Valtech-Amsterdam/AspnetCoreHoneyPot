﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Models;

namespace Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Services
{
    /// <summary>
    /// A default implementation of the AspnetCoreHoneyPotService
    /// </summary>
    public class DefaultHoneyPotService : IHoneyPotService
    {
        /// <summary>
        /// DefaultHoneyPotServiceSessionInputIdentifier
        /// </summary>
        public static readonly string DefaultHoneyPotServiceSessionInputIdentifier =
            nameof(DefaultHoneyPotServiceSessionInputIdentifier);
        /// <summary>
        /// Handle for the randomizer
        /// </summary>
        protected readonly Random _randomizer;
        /// <summary>
        /// Handle for the current HttpContext
        /// </summary>
        protected readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// A default implementation of the AspnetCoreHoneyPotService
        /// </summary>
        public DefaultHoneyPotService(IHttpContextAccessor httpContextAccessor)
        {
            _randomizer = new Random();
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// The Settings of this HoneyPotService
        /// </summary>
        public virtual HoneyPotSettings Settings => new HoneyPotSettings();

        /// <summary>
        /// The identifier currently selected for the HoneyPot input field
        /// </summary>
        public virtual string SelectedInputIdentifier => _httpContextAccessor.HttpContext?.Session?
            .GetString(DefaultHoneyPotServiceSessionInputIdentifier);

        /// <summary>
        /// Generate a random identifier for the HoneyPot Input field
        /// </summary>
        /// <returns></returns>
        public virtual string GetRandomInputIdentifier()
        {
            if (_httpContextAccessor.HttpContext?.Session == null) return "NoSession";

            var randomizedIdentifiers = Settings.InputIdentifiers.OrderBy(x => _randomizer.Next());
            var left = randomizedIdentifiers.First();
            var right = randomizedIdentifiers.Skip(1).First();
            var inputIdentifier = $"{left}-{right}";

            _httpContextAccessor.HttpContext.Session.SetString(DefaultHoneyPotServiceSessionInputIdentifier, inputIdentifier);

            return inputIdentifier;
        }

        /// <summary>
        /// Return an error message
        /// </summary>
        /// <returns></returns>
        public virtual string GetErrorMessage() => "You might be a bot!";
    }
}
