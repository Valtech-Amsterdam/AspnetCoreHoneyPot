# Contribution guide  
  
  
## Contributing
If you like to contribute you're welcome please note that releases are constructed based on issues in this project.  
If an issue is unassigned you can assign it to yourself and start later.  
If you want to apply a fix for a bug please create an issue and assign it to yourself first.  
  

If you don't create a branch from the issue please add a reference to the issue in the merge request.  
  

When your change is done please create a merge request to the release branch the ticket is assigned to,   
There is always a release branch if the release milestone exists.  
If the change is valid you are requested to merge the branch yourself and close the issue once the merge is complete.  
  

## Branching model
The branching model we use is called "tag-flow" (blog incoming...), the tag-flow is constructed out of 4 simple steps:  

1. Create release  
   Create a branch with name "release/{Major}.{Minor}.{Patch}" and set a milestone for this release.  
   Creating a release branch will automatically build alpha packages for every merge.
2. Decide release contents  
   Assign milestones to the issues you want to add to this release
3. Resolve issues  
   Contributors assigned to issues create branches regarding issues and will create merge requests to the release branch matching the issues milestone.  
   If all the issues assigned tot the milestone are resolved the release can be started.
4. Release  
   Only masters can release, you will be assigned a release by the master currently managing the repository.  
   Merge the release branch back into master and tag the master with the release version "tag/{Major}.{Minor}.{Patch}".  
   The release will be pushed to nuget automatically.