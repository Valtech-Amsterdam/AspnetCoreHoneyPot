<table style="border: 0px transparent none !important;">
  <tbody style="border: 0px transparent none !important;">
    <tr style="border: 0px transparent none !important;">
      <td  style="border: 0px transparent none !important;" align="center">
        <img style="display: inline-block; margin: 0 auto 0 auto" width="200" src="documentation/AspnetCoreHoneyPot.png" />
      </td>
    </tr>
    <tr style="border: 0px transparent none !important;">
      <td  style="border: 0px transparent none !important;" align="center">
        <a style="display: inline-block; margin: 0 auto 0 auto; width:100%; heigth:100%;" href="https://gitlab.com/Valtech-Amsterdam/MvcHoneyPot">
          For the AspNet MVC version see our AspnetCoreHoneyPot package
        </a>
      </td>
    </tr>
  </tbody>
</table>

# AspnetCoreHoneyPot
A honeypot like [SimpleHoneypot](https://github.com/webadvanced/Honeypot-MVC) but with a sligtly different philosophy.  
Also it still works in mvc5!  
<p>&nbsp;</p>
<img style="display: inline-block; margin: 0 auto 0 auto" width="350" src="http://i.giphy.com/C6JQPEUsZUyVq.gif" /> 

## What is a HoneyPot?
<p>In essence it's just a hidden field used to trick a simple spambot into filling in a field that it's not supposed to.<p>  
<p>&nbsp;</p>  
<table>
  <tbody>
    <tr>
      <td> 
        <q>In computer terminology, a <b>honeypot</b> is a <a target="_blank" href="https://en.wikipedia.org/wiki/Computer_security" title="Computer security">computer security</a> mechanism set to detect, 
           deflect, or, in some manner, counteract attempts at unauthorized use of <a target="_blank" href="https://en.wikipedia.org/wiki/Information_system" class="mw-redirect" title="Information systems">information systems</a>. 
           Generally, a honeypot consists of <a target="_blank" href="https://en.wikipedia.org/wiki/Data" title="Data">data</a> (for example, in a network site) that appears to be a legitimate part of the site but is actually isolated and monitored, 
           and that seems to contain information or a resource of value to attackers, which are then blocked. 
           This is similar to the police <a target="_blank" href="https://en.wikipedia.org/wiki/Bait_(luring_substance)" title="Bait (luring substance)">baiting</a> a criminal and then conducting undercover <a target="_blank" href="https://en.wikipedia.org/wiki/Surveillance" title="Surveillance">surveillance</a>, 
           and finally punishing the criminal.<sup id="cite_ref-1" class="reference"><a target="_blank" href="https://en.wikipedia.org/wiki/Honeypot_(computing)#cite_note-1">[1]</a></sup></q>  
      </td>
    </tr>
    <tr>
      <td style="font-size:8pt;">  
        <i>Source: <a target="_blank" href="https://en.wikipedia.org/wiki/Honeypot_(computing)">Wikipedia: Honeypot_(computing) | 13-9-2016</a></i>
      </td>
    </tr>
  </tbody>
</table>

## Why not use (re)captcha?
There's no apparent reason why not, also there's no reason why you shouldn't use both.  
The advantage of using this library is that it requires very little time to set up and it's very clean to customize functionalities like localization.  
It's cheap and simple to implement and it's not obtrusive for site visitors.

## How does it differ from [SimpleHoneypot](https://github.com/webadvanced/Honeypot-MVC)?
[AspnetCoreHoneyPot](https://gitlab.com/Valtech-Amsterdam/AspnetCoreHoneyPot) has a few slight differences from [SimpleHoneypot](https://github.com/webadvanced/Honeypot-MVC).

### Handling possible bots
The main difference is that [SimpleHoneypot](https://github.com/webadvanced/Honeypot-MVC) is the way possible bot's are handled.
[SimpleHoneypot](https://github.com/webadvanced/Honeypot-MVC) redirects to a page if you are detected as a bot, 
not only is this confusing for users if not configured but it's also a pain to have to configure it on every attribute validating the request.  
If you want to show a message instead of redirecting you have to override this in every post call.  
  
The [AspnetCoreHoneyPot](https://gitlab.com/Valtech-Amsterdam/AspnetCoreHoneyPot) solution just adds a message to your modelstate, giving the user more context of what is happening.  
Also this is much easier to override just by registering your own implementation, this gives opportunities to implement localization providers for example.  

### Implementing and customizing the library
The way you implement [SimpleHoneypot](https://github.com/webadvanced/Honeypot-MVC) is by having an injected class with some seemingly static/generatied code in your App_data folder.  
  
The way you implement [AspnetCoreHoneyPot](https://gitlab.com/Valtech-Amsterdam/AspnetCoreHoneyPot) is by registering it in your IOC container, this opens op the possibility to use your custom implementation like described above.  
For an example of how to do this you can consult the example project in this repository.

## Installing the library
To install the library just use:  
`PM> Install-Package Valtech.Amsterdam.Labs.AspnetCoreHoneyPot`  
And install the latest version.  
  
## Prerequisites  
The library requires the IHttpContextAccessor to be available and it requires the Session to be configured.  
For a good example on using sessions in an Aspnet-Core solution see: [Using Sessions and HttpContext in ASP.NET Core and MVC Core](http://benjii.me/2016/07/using-sessions-and-httpcontext-in-aspnetcore-and-mvc-core/).
  
## Configuration
You will need to register the honeypotservice in your IOC Container as well as add the Actionfilter to the mvc configuration.  
This can be done in the Startup.cs:  
```cs
    public void ConfigureServices(IServiceCollection services)
    {
        // ...
        services.AddHoneyPot();
        services.AddMvc(config => config.AddHoneyPot());
        // ...
    }
```  
You can override the DefaultHoneyPotService to change some settings, see the example project for a usefull example.  
${SolutionDir}\src\AspnetCoreHoneyPot.Example\Services\HoneyPot\CustomHoneyPotService.cs.  
  
To use the honeypot include the following line inside a form on your cshtml file:  
```cs
@Html.HoneyPot()
```  
Using this namespace:  
```cs
@using Valtech.Amsterdam.Labs.AspnetCoreHoneyPot.Extensions
```  
And that's it, the actionfilter will check the honeypot automatically.  

## [Contributing](Contributing.md)
See the [Contribution guide](Contributing.md) for help about contributing to this project.
  
## [Changelog](Changelog.md)
See the [Changelog](Changelog.md) to see the change history.