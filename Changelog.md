# Changelog

## Release 1.0.1 (latest)
* Moved to library project
* Moved to dotnet-core 1.1

## Release 1.0.0
* Forked [MvcHoneyPot](https://gitlab.com/Valtech-Amsterdam/MvcHoneyPot) and converted it to AspNetCore